﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Contracts;
using Domain;
using System.ComponentModel.Composition;
using DataAccess.Interfaces;
using System.Security.Permissions;
using Domain.SubDomain;

namespace RoutePlannerServices
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, ReleaseServiceInstanceOnTransactionComplete = false)]
    public class CruiseManager : ServicesBase, ICruiseContract
    {

        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        string UploadFolder = "c:\\Static\\";

        public CruiseManager() { }

        public CruiseManager(IDataRepositoryFactory DataRepositoryFactory) // for unit tests. WCF will NOT run this!!!
        {
            _DataRepositoryFactory = DataRepositoryFactory;
        }

        public void DeleteCruise(int id)
        {
            ExecuteFaultHandledOperation(() =>
            {
                ICruiseRepository CruiseRepo = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>();
                ValidateAutorizationAccount(CruiseRepo.Get(id));

                IPhotoRepository PhotoRepo = _DataRepositoryFactory.GetDataRepository<IPhotoRepository>();
                IStayRepository StayRepo = _DataRepositoryFactory.GetDataRepository<IStayRepository>();
                PhotoRepo.DeleteAllEntitiesByCruiseId(id);
                StayRepo.DeleteAllEntitiesByCruiseId(id);
                CruiseRepo.Remove(id);
                Cruise.DeleteFolder(UploadFolder + id.ToString());

            });
        }

        public Cruise[] GetAllCruises(int CurrentPage)
        {
            //var PrimaryIdentityName = OperationContext.Current.ServiceSecurityContext.PrimaryIdentity.Name;
            //var AuthenticationType = OperationContext.Current.ServiceSecurityContext.PrimaryIdentity.AuthenticationType;

            //var WindowsIdentityName = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.Name;
            //var AuthenticationTypeWindowsIdentity = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.AuthenticationType;
            //var WindowsIdentityUser = OperationContext.Current.ServiceSecurityContext.WindowsIdentity.User.ToString();

            
            return ExecuteFaultHandledOperation(() =>
            {
                IEnumerable<Cruise> allCruises = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>().GetAll(CurrentPage);

                //foreach (var cruise in allCruises)
                //{
                //    cruise.CheckIfPhotoExist();
                //}

                return allCruises.ToArray();
            });
        }

        public Cruise GetCruiseById(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Cruise cruise = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>().Get(id);
                if (cruise == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format("Cruise with Id of {0} not found", id));
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }
               // cruise.LoadPhotos(UploadFolder);
                return cruise;
            });
        }

        public Cruise GetCruises(string login)
        {
            throw new NotImplementedException();
        }

        public Cruise[] GetCruisesByAccount(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IEnumerable<Cruise> Cruises = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>().GetByAccountId(id);
                foreach (var cruise in Cruises)
                {
                    cruise.CheckIfPhotoExist();
                }

                return Cruises.ToArray();
            });
        }


        [OperationBehavior(TransactionScopeRequired = true)]
        //[PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        //[PrincipalPermission(SecurityAction.Demand, Name = Security.RoutePlannerUser)]
        public Cruise UpdateCruise(Cruise Cruise)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                ICruiseRepository CruiseRepo = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>();
                Cruise updatedCruise = null;
                if (Cruise.CruiseId == 0)
                    updatedCruise = CruiseRepo.Add(Cruise);
                else
                {
                    ValidateAutorizationAccount(CruiseRepo.Get(Cruise.CruiseId));
                    updatedCruise = CruiseRepo.Update(Cruise);
                }

                return updatedCruise;
            });
        }

        [OperationBehavior(TransactionScopeRequired = true)]
        //[PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        //[PrincipalPermission(SecurityAction.Demand, Name = Security.RoutePlannerUser)]
        public void UpdateStays (Cruise Cruise)
        {
             ExecuteFaultHandledOperation(() =>
            {
                var CruiseRepo = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>(); 
                ValidateAutorizationAccount(CruiseRepo.Get(Cruise.CruiseId));
                IStayRepository StayRepo = _DataRepositoryFactory.GetDataRepository<IStayRepository>();
                StayRepo.DeleteAllEntitiesByCruiseId(Cruise.CruiseId);
                StayRepo.AddEntitiesWithCruiseId(Cruise.Stays,Cruise.CruiseId);
               // return updatedCruise;
            });
        }

        public void DeletePhoto(Photo Photo)
        {
            ExecuteFaultHandledOperation(() => {
                var PhotoRepo = _DataRepositoryFactory.GetDataRepository<IPhotoRepository>();
                ValidateAutorizationAccount(PhotoRepo.Get(Photo.PhotoId));
                PhotoRepo.Remove(Photo);
                Photo.Delete(UploadFolder);
            });
        }

        public void SetMainPhoto(Photo photo)
        {
            ExecuteFaultHandledOperation(() => {
                var PhotoRepo = _DataRepositoryFactory.GetDataRepository<IPhotoRepository>();
                ValidateAutorizationAccount(PhotoRepo.Get(photo.PhotoId));
                foreach (var _photo in PhotoRepo.GetAllEntitiesByCruiseId(photo.CruiseId).Where(p=>p.IsMain==true))
                    {
                        _photo.IsMain = false;
                        PhotoRepo.Update(_photo);
                    }
                PhotoRepo.Update(photo);
            });
        }

        public int GetAllCruisesNumber()
        {
           return ExecuteFaultHandledOperation(() => {
                ICruiseRepository CruiseRepo = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>();
            return CruiseRepo.GetAll();
            });
        }
    }
}
