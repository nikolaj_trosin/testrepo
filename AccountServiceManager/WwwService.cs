﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using Contracts;

namespace RoutePlannerServices
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class WwwServiceImplementation :  IWwwService
    {
        [WebInvoke(Method = "GET",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/{*content}")]
        public Stream StaticContent(string content)
        {//"www/"
            if (string.IsNullOrEmpty(content))
                return null;
            string FileName = content.Substring(8);
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            //  string path = "D://Static//1//"  + (string.IsNullOrEmpty(FileName) ? "index.html" : content);
            string path = "C://Static//" + FileName;
            string extension;
            try {extension = Path.GetExtension(path); }
            catch (Exception ex) { return null; }
            string contentType = string.Empty;

            switch (extension)
            {
                case ".htm":
                case ".html":
                    contentType = "text/html";
                    break;
                case ".jpg":
                    contentType = "image/jpeg";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".ico":
                    contentType = "image/x-icon";
                    break;
                case ".js":
                    contentType = "application/javascript";
                    break;
                case ".json":
                    contentType = "application/json";
                    break;
            }

            if (File.Exists(path) && !string.IsNullOrEmpty(contentType))
            {
                response.ContentType = contentType;
                response.StatusCode = System.Net.HttpStatusCode.OK;
                var filestream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                MemoryStream temp = new MemoryStream();
                filestream.CopyTo(temp);
                temp.Position = 0;
                filestream.Dispose();
                return temp;
                
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
        }
    }
}