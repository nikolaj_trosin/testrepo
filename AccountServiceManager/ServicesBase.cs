﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Domain;
using Contracts;
using Domain.Interfaces;
using DataAccess;
using DataAccess.Interfaces;
using System.ServiceModel;
using System.Threading;

namespace RoutePlannerServices
{
    public class ServicesBase
    {
        public ServicesBase()
        {
            OperationContext context = OperationContext.Current;
            if (context != null)
            {
                LoginName = context.IncomingMessageHeaders.GetHeader<string>("String", "System");
                if (LoginName.IndexOf(@"\") > 1) LoginName = String.Empty;
            }

            if (DomainBase.Container!=null) 
            DomainBase.Container.SatisfyImportsOnce(this);  // post constraction resolve by MEF because Service served by WCF not MEF

            if (!String.IsNullOrWhiteSpace(LoginName))
            {
                AutorizationAccount = new Account();
                AutorizationAccount.AccountId = LoadAutorizationValidationAccount(LoginName);

            }
        }

        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;
        protected string LoginName;
        protected Account AutorizationAccount = null;

        protected virtual int LoadAutorizationValidationAccount (string login)
        {
            using (Entities context = new Entities())
            {
                var user = context.AspNetUsers.Where(u => u.Email == login).SingleOrDefault();
                if (user == null)
                {
                    AuthorizationValidationException ex = new AuthorizationValidationException("Attempt to access a secure record with improper user authorization validation.");
                    throw new FaultException<AuthorizationValidationException>(ex, ex.Message);
                }
                    
                return user.AccountID;
            }
        }

        protected void ValidateAutorizationAccount(IAccountOwnedEntity entity)
        {
            //if (!Thread.CurrentPrincipal.IsInRole(Security.RoutePlannerAdmin))
            //{
            
            if (AutorizationAccount!=null)
                {
                    if (LoginName!=string.Empty && entity.OwnerAccountId!= AutorizationAccount.AccountId)
                    {
                        AuthorizationValidationException ex = new AuthorizationValidationException("Attempt to access a secure record with improper user authorization validation.");
                        throw new FaultException<AuthorizationValidationException>(ex,ex.Message);
                    }
                }
            if(String.IsNullOrEmpty(LoginName) || String.IsNullOrWhiteSpace(LoginName) || AutorizationAccount==null)
            {
                AuthorizationValidationException ex = new AuthorizationValidationException("Attempt to access a secure record with improper user authorization validation.");
                throw new FaultException<AuthorizationValidationException>(ex, ex.Message);
            }
            //}
        }

        protected T ExecuteFaultHandledOperation<T>(Func<T> codetoexecute)
        {
            try
            {
                return codetoexecute.Invoke();
            }
            catch(FaultException ex)
            {
                throw ex;
            }
            catch(Exception ex)
            {
                throw new FaultException(ex.Message);
            }

        }

        protected void ExecuteFaultHandledOperation(Action codetoexecute)
        {
            try
            {
                codetoexecute.Invoke();
            }
            catch (FaultException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }

        }


    }
}
