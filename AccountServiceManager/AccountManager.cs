﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Domain;
using DataAccess.Interfaces;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Threading;
using Contracts;

namespace RoutePlannerServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.

    /// <summary>
    ///  063 explanation!!!
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, ReleaseServiceInstanceOnTransactionComplete = false)]
    public class AccountManager : ServicesBase, IAccountContract
    {
        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        public AccountManager() { } // default constructor

        public AccountManager(IDataRepositoryFactory DataRepositoryFactory) // for unit tests. WCF will NOT run this!!!
        {
            _DataRepositoryFactory = DataRepositoryFactory;
        }


        protected override int LoadAutorizationValidationAccount(string login)
        {
            IAccountRepository AccountRepo = _DataRepositoryFactory.GetDataRepository<IAccountRepository>();
            Account AutorizationAccount = AccountRepo.GetByLogin(login);

            if (AutorizationAccount == null)
                throw new NotFoundException(String.Format("Can not find account for login {0}", login));

            return AutorizationAccount.AccountId;
        }

        [OperationBehavior(TransactionScopeRequired = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        public void DeleteAccount(int id)
        {
            ExecuteFaultHandledOperation(() =>
            {
                IAccountRepository accountRepo = _DataRepositoryFactory.GetDataRepository<IAccountRepository>();
                accountRepo.Remove(id);
            });
        }

        [PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        [PrincipalPermission(SecurityAction.Demand, Name = Security.RoutePlannerUser)]
        public Account GetAccountById(int id)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Account account = _DataRepositoryFactory.GetDataRepository<IAccountRepository>().Get(id);
                if (account == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format("Account with Id of {0} not found",id));
                    throw new FaultException<NotFoundException>(ex,ex.Message);
                }
                ValidateAutorizationAccount(account);

                return account;
            });
        }


        [PrincipalPermission(SecurityAction.Demand, Role =Security.RoutePlannerAdmin) ]
        public Account[] GetAllAccounts()
        {

            return ExecuteFaultHandledOperation(() =>
            {
                IEnumerable<Account> allAccounts = _DataRepositoryFactory.GetDataRepository<IAccountRepository>().Get();
                return allAccounts.ToArray();
            });
        }

        [OperationBehavior(TransactionScopeRequired =true)]
        [PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        [PrincipalPermission(SecurityAction.Demand, Name = Security.RoutePlannerUser)]
        public Account UpdateAccount(Account account)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IAccountRepository accountRepo = _DataRepositoryFactory.GetDataRepository<IAccountRepository>();
                Account updatedAccount = null;
                if (account.AccountId == 0)
                    updatedAccount = accountRepo.Add(account);
                else
                    updatedAccount = accountRepo.Update(account);

                ValidateAutorizationAccount(account);
        

                return updatedAccount;
            });

        }


        [PrincipalPermission(SecurityAction.Demand, Role = Security.RoutePlannerAdmin)]
        [PrincipalPermission(SecurityAction.Demand, Name = Security.RoutePlannerUser)]
        public Account GetAccount(string login)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                Account account = _DataRepositoryFactory.GetDataRepository<IAccountRepository>().GetByLogin(login);
                if (account == null)
                {
                    NotFoundException ex = new NotFoundException(string.Format("Account with Id of {0} not found", account.AccountId));
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }

                ValidateAutorizationAccount(account); 

               
                return account;
            });
        }

    }
}
