﻿using Contracts;
using DataAccess.Interfaces;
using Domain.SubDomain;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace RoutePlannerServices
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, ReleaseServiceInstanceOnTransactionComplete = false)]
    public class FileManager: ServicesBase, IFileContract
    {
        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        [OperationBehavior(TransactionScopeRequired = true)]
        public void GetFile(FileUploadMessage myData)
        {
            try
            {
                Stream sourceStream = myData.FileByteStream;
                FileStream targetStream = null;

                string uploadFolder = @"C:\Static\"+myData.AuthCode;
                System.IO.Directory.CreateDirectory(uploadFolder);

              //  var CruiseRepo = _DataRepositoryFactory.GetDataRepository<ICruiseRepository>();
                var PhotoRepo = _DataRepositoryFactory.GetDataRepository<IPhotoRepository>();

                Photo photo = new Photo();
                photo.CruiseId = int.Parse(myData.AuthCode);
                photo.AccountId = myData.AccountId;
                photo.FileName = Path.GetRandomFileName() + ".jpg";
                ValidateAutorizationAccount(photo);

                //  var cruise = CruiseRepo.Get(int.Parse(myData.AuthCode));
                string filePath = Path.Combine(uploadFolder, photo.FileName);

                using (targetStream = new FileStream(filePath, FileMode.Create,
                              FileAccess.Write, FileShare.None))
                {
                    //  CruiseRepo.Update(cruise);
                    PhotoRepo.Add(photo);

                    //read from the input stream in 4K chunks
                    //and save to output stream
                    const int bufferLen = 4096;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        targetStream.Write(buffer, 0, count);
                    }
                    targetStream.Close();
                    sourceStream.Close();
                }


            }
            catch (Exception ex)
            {

                throw new FaultException(ex.Message);
            }
        } // make ExecuteFaultHandledOperation later!!!!
    }
}
