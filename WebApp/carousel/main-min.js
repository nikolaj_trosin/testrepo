// global-------------------------------------

$(function ($) {
    $("#carousel-featured").owlCarousel({
        items: 1,
        dots: false,
        nav: true,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplayHoverPause: true,
        autoplaySpeed: 500,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        mouseDrag: false,
        loop: true
    });

    $("#cartCarousel").owlCarousel({
        items: 3,
        margin: 10,
        nav: true,
        responsive: {
            1200: {
                items: 4
            },
            970: {
                items: 3
            },
            768: {
                items: 2
            }
        }
    });

    $(".gallery-carousel .owl-carousel").owlCarousel({
        center: true,
        dots: false,
        items: 3,
        nav: true,
        margin: 10,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 6000,
        autoplaySpeed: 500,
        loop: true,
        responsive: {
            1200: {
                items: 5
            },
            970: {
                items: 5
            },
            768: {
                items: 3
            }
        }
    });
    $(".gallery-carousel .owl-carousel").each(function () {
        var carousel = $(this);
        var owl = carousel.owlCarousel();
        $(".owl-item", carousel).click(function () {
            var parent = this.parentElement;

            for (var i = 0; i < parent.childElementCount; i++) {
                if (parent.childNodes[i] === this) {
                    carousel.trigger("to.owl.carousel", [i]);
                    break;
                }
            }
        });

        carousel.on("changed.owl.carousel", function (e) {
            var item = $(".owl-item", carousel)[e.property.value];

            var divs = $(" > div > div", item).each(function () {
                var div = $(this);

                var target = $("#" + div.attr("class"));
                var data = div.html();
                target.html(data);
            });

            var Height1 = $('.gallery-carousel .owl-carousel').height()
            var Height2 = $('.gallery-carousel .navi-container').height()
            var Height3 = $('.gallery-carousel .text').height()
            var Height4 = $('.gallery-carousel .link').height()
            var totalHeight = Height1 + Height2 + Height3 + Height4 + 160;
            $('.gallery-carousel').css('height', totalHeight);
        });
    });
    $(".navi.right").click(function () {
        var target = $("#" + $(this).data("target"));
        target.trigger("next.owl.carousel");
    });
    $(".navi.left").click(function () {
        var target = $("#" + $(this).data("target"));
        target.trigger("prev.owl.carousel");
    });
});

// menu epxand

$(".category-trigger").bind('touchstart click', function (event) {
    event.preventDefault();
    if ($('#menuExpand').hasClass("active")) {
        $('#menuExpand').removeClass("active");

    } else {
        $('#menuExpand').addClass("active");
        $('#Minicart').removeClass("active");
    }
    return false;
});

$("#menuExpand .menu li a, #menuExpand .sub-item .login,#menuExpand .close").bind('click', function (event) {
    if ($('#menuExpand').hasClass("active")) {
        $('#menuExpand').removeClass("active");

    } else {
        $('#menuExpand').addClass("active");
    }
});

$("#cart").bind('touchstart click', function (event) {
    event.preventDefault();
    if ($('#Minicart').hasClass("active")) {
        $('#Minicart').removeClass("active");

    } else {
        $('#Minicart').addClass("active");
        $('#menuExpand').removeClass("active");
    }
    return false;
});

$("#Minicart .close").bind('touchstart click', function (event) {
    event.preventDefault();
    if ($('#Minicart').hasClass("active")) {
        $('#Minicart').removeClass("active");

    } else {
        $('#Minicart').addClass("active");
        $('#menuExpand').removeClass("active");
    }
    return false;
});
// fire goup


$(document).ready(function () {
    $.goup({
        trigger: 400,
        bottomOffset: 100,
        locationOffset: 27
    });
});

$(document).ready(function () {

    var footerHeight = $('#footer').css("height");
    $('#social').css('margin-bottom', footerHeight);
    $(window).on("resize", function () {
        var footerHeight = $('#footer').css("height");
        $('#social').css('margin-bottom', footerHeight);
    });
});

$(document).ready(function () {
    $(".gif-poster").hover(
        function () {
            var src = $(this).attr("src");
            $(this).attr("src", src.replace(/\.jpg$/i, ".gif"));
        },
        function () {
            var src = $(this).attr("src");
            $(this).attr("src", src.replace(/\.gif$/i, ".jpg"));
        });
});

// home-------------------------------------

// homeslide auto play


// boostrap tab

$('a.part-category').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})

$('.nav-tabs a:first').tab('show');

$(".dropdown-menu li a").click(function () {
    var selText = $(this).text();
    $(this).parents('.dropdown').find('.dropdown-toggle').html(selText);
});
