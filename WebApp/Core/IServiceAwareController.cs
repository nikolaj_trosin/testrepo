using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;

namespace WebApp.Core
{
    public interface IServiceAwareController
    {
        void RegisterDisposableServices(List<IServiceProxyContract> disposableServices);

        List<IServiceProxyContract> DisposableServices { get; }
    }
}
