﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain;

namespace WebApp.Models
{
    public class HomeViewModel
    {
        public List<Cruise> CruisesHomePage { get; set; }
    }
}