﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Proxies;
using Contracts;
using Domain;
using WebApp.Core;

namespace WebApp.Controllers.API
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [RoutePrefix("api/account")]
    [Authorize]
    [UsesDisposableService]
    public class AccountApiController : ApiControllerBase
    {

        [ImportingConstructor]
        public AccountApiController(IAccountContract accountService) // just an example, Account stuff  will conect to DB without WCF proxy
        {
            AccountService = accountService;
        }
        // 163
        protected override void RegisterServices(List<IServiceProxyContract> disposableServices)
        {
            disposableServices.Add(AccountService);
        }


        IAccountContract AccountService;

        [HttpGet]
        [Route("logins")] // example of proxy usage
        public HttpResponseMessage Logins(HttpRequestMessage request, [FromUri] string dd) // example of proxy usage
        {
            return GetHttpResponse(request, () => {
               Account account =  AccountService.GetAccountById(4);
                return request.CreateResponse<Account>(HttpStatusCode.OK, account);
            });

        }


        [HttpPost]
        [Route("login")]
        public HttpResponseMessage Login(HttpRequestMessage request,[FromBody] string accountbodel) // [FromBody] to get view model from frontend see 133
        {

            return GetHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                bool success = false; // =  SecurytyAdapter.Login(accountmodel);
                if (success)
                    request.CreateResponse(HttpStatusCode.OK); // see 134-135!!!
                else
                    response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized!!!!!!!!!!!!!!");

                return response;
            });
        }
    }
}
