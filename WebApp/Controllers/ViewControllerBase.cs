﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Contracts;
namespace WebApp.Controllers
{
    public class ViewControllerBase: Controller
    {
        List<IServiceProxyContract> _DisposableServices;

        protected virtual void RegisterServices(List<IServiceProxyContract> disposableServices)
        {
        }

        List<IServiceProxyContract> DisposableServices
        {
            get
            {
                if (_DisposableServices == null)
                    _DisposableServices = new List<IServiceProxyContract>();

                return _DisposableServices;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            RegisterServices(DisposableServices);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            foreach (var service in DisposableServices)
            {
                if (service != null && service is IDisposable)
                    (service as IDisposable).Dispose();
            }
        }
    }
}