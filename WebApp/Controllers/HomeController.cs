﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.SubDomain;
using Contracts;
using WebApp.Models;
using System.Runtime.Serialization;
using System.IO;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Mail;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using Bing;
using System.Net;
using System.Configuration;

namespace WebApp.Controllers
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class HomeController : ViewControllerBase
    {
      //  IAccountContract AccountService;
        ICruiseContract CruiseService;
        IFileContract FileService;

        public string Address { get; set; }

        [ImportingConstructor]
        public HomeController( ICruiseContract cruiseService, IFileContract fileService)
        { // changes for test commit from VS
           // AccountService = accountService;
            CruiseService = cruiseService;
            FileService = fileService;
            Address = ConfigurationManager.AppSettings.Get("ip");
        }

        protected override void RegisterServices(List<IServiceProxyContract> disposableServices)
        {
            disposableServices.Add(CruiseService);
            disposableServices.Add(FileService);
        }

        [HttpGet]
        public ActionResult Index() // implement service disposibilyty 
        {
            ViewBag.CurrentPage = 1;
            ViewBag.LastPage = Math.Ceiling(Convert.ToDouble(CruiseService.GetAllCruisesNumber())/5);
            ViewBag.Address = Address;
            HomeViewModel homeVeiwModel = new HomeViewModel();
            homeVeiwModel.CruisesHomePage = CruiseService.GetAllCruises(1).ToList();                 
            return View(homeVeiwModel);
        }

        [HttpPost]
        public ActionResult Index(int CurrentPage, int LastPage)  
        {
            ViewBag.CurrentPage = CurrentPage;
            ViewBag.LastPage = LastPage;
            ViewBag.Address = Address;

            HomeViewModel homeVeiwModel = new HomeViewModel();
            homeVeiwModel.CruisesHomePage = CruiseService.GetAllCruises(CurrentPage).ToList();
            return View(homeVeiwModel);
        }

        [HttpGet]
        public ActionResult Cruise(int id)
        {
            ViewBag.Address = Address;
            Cruise cruise = CruiseService.GetCruiseById(id);
            return View(cruise);

        }

        [HttpPost]
        [Authorize]
        public JsonResult PostStays(IList<double> staysCoord, IList<double> curves, IList<double> altStaysCoord)
        {

            if (staysCoord == null)
                return null;
            IEnumerable<string> headerValues = HttpContext.Request.Headers.GetValues("cruise_id");

            CruiseService.UpdateStays( AddStaysToCruise(staysCoord, curves, altStaysCoord, int.Parse(headerValues.FirstOrDefault())));

            return Json(new { value = "Your Route has been saved." });
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetCruise(int id)
        {
            ViewBag.Address = Address;
            var user = HttpContext.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindById(User.Identity.GetUserId());
            Cruise cruise = CruiseService.GetCruiseById(id);

            if (cruise.OwnerAccountId != user.AccountID)
                return null; // return Cruise( action without edit fuctionality) later!

            return View(cruise);

        }

        [HttpPost]
        [Authorize]
        public ActionResult GetCruise(Cruise cruise)
        {
            //var user = HttpContext.GetOwinContext()
            //               .GetUserManager<ApplicationUserManager>()
            //               .FindById(User.Identity.GetUserId());
            //Cruise cruise = CruiseService.GetCruiseById(id);

            //if (cruise.OwnerAccountId != user.AccountID)
            //    return null; // return Cruise( action without edit fuctionality) later!
            CruiseService.UpdateCruise(cruise);
             // return RedirectToAction("GetCruise", "Home", new { id = cruise.CruiseId });
            return PartialView("_PartialGetCruise",cruise);
        }


        [HttpGet]
        [Authorize]
        public ActionResult MyCruises()
        {
            var user = HttpContext.GetOwinContext()
               .GetUserManager<ApplicationUserManager>()
               .FindById(User.Identity.GetUserId());
            ViewBag.Address = Address;
            HomeViewModel homeVeiwModel = new HomeViewModel();
            homeVeiwModel.CruisesHomePage = CruiseService.GetCruisesByAccount(user.AccountID).ToList();
            return View(homeVeiwModel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult CreateCruise()
        {

            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult CreateCruise(Cruise cruise)
        {
            cruise.AccountId = HttpContext.GetOwinContext()
                 .GetUserManager<ApplicationUserManager>()
                 .FindById(User.Identity.GetUserId()).AccountID;

            var new_cruise = CruiseService.UpdateCruise(cruise);

            return RedirectToAction("GetCruise", "Home", new { id = new_cruise.CruiseId });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [Authorize]
        public string UploadFile()
        {
            int AccountId = HttpContext.GetOwinContext()
                 .GetUserManager<ApplicationUserManager>()
                 .FindById(User.Identity.GetUserId()).AccountID;

            HttpPostedFile pic;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
            }
            else { return null; }
            //StreamReader reader = new StreamReader(pic.InputStream,System.Text.Encoding.ASCII);
            //var text = reader.ReadToEnd();

            byte[] a;

            using (var memoryStream = new MemoryStream())
            {
                pic.InputStream.Position = 0;
                pic.InputStream.CopyTo(memoryStream);
                a = memoryStream.ToArray();
            }
            FileUploadMessage file_message = new FileUploadMessage();
            MemoryStream stream = new MemoryStream();
            stream.Write(a, 0, a.Length);
            stream.Seek(0, SeekOrigin.Begin);
            file_message.AccountId = AccountId;
            file_message.FileByteStream = stream;
            file_message.FileName = pic.FileName;
            IEnumerable<string> headerValues = HttpContext.Request.Headers.GetValues("cruise_id");
      
            file_message.AuthCode = headerValues.FirstOrDefault();
            FileService.GetFile(file_message);

            string str = Convert.ToBase64String(a);
            var return_string = "data:image/png;base64," + str;

            //   bool isUploaded = true;
            //string   message = "File uploaded successfully!";
            return return_string;//Json(new { isUploaded = isUploaded, message = message }, "text/html");
        }

        [HttpPost]
        [Authorize]
        public void DeletePhoto(int CruiseId, int PhotoId, string FileName)
        {
            Photo photo= new Photo();
            photo.PhotoId = PhotoId;
            photo.CruiseId = CruiseId;
            photo.FileName = FileName;
            CruiseService.DeletePhoto(photo);
        }

        [HttpPost]
        [Authorize]
        public void SetMainPhoto(int CruiseId, int PhotoId, string FileName)
        {
            Photo photo = new Photo();
            photo.AccountId = HttpContext.GetOwinContext()
                 .GetUserManager<ApplicationUserManager>()
                 .FindById(User.Identity.GetUserId()).AccountID;
            photo.PhotoId = PhotoId;
            photo.CruiseId = CruiseId;
            photo.FileName = FileName;
            photo.IsMain = true;
            CruiseService.SetMainPhoto(photo);
        }

        [HttpPost]
        public void SendEmailToCruiseOwner(int CruiseId, int AccountId, string Email, string Name,string Message, bool SendToMe)
        {
            if (String.IsNullOrEmpty(Email) || String.IsNullOrEmpty(Name) || String.IsNullOrEmpty(Message))
                return;
            var user = HttpContext.GetOwinContext()
                           .GetUserManager<ApplicationUserManager>()
                           .Users.Where(u=>u.AccountID == AccountId).FirstOrDefault();

            var myMessage = new SendGrid.SendGridMessage();
            myMessage.AddTo(user.Email);
            myMessage.From = new MailAddress(Email, Name);
            myMessage.Subject = "Message from "+Name;
            myMessage.Text = Message;

            var transportWeb = new SendGrid.Web(ConfigurationManager.AppSettings.Get("mailPassword"));

            if (SendToMe)
            {
                var SendToMeMessage = new SendGrid.SendGridMessage();
                SendToMeMessage.AddTo(Email);
                SendToMeMessage.From = new MailAddress("joe@findacruise.com", "Joe S.");
                SendToMeMessage.Subject = "Your Message to Cruise owner was delivered";
                var fullUrl = this.Url.Action("Home", "Cruise", new { id = CruiseId }, this.Request.Url.Scheme);
                SendToMeMessage.Text = "Cruise "+ fullUrl +  " Your Message to Cruise owner: " + Message;
                SendToMeMessage.Html = "<h4>Cruise:&nbsp;" + fullUrl + "</h4> <h4> Your Message to Cruise owner:&nbsp;</h4><br />" + Message;
                transportWeb.DeliverAsync(SendToMeMessage).Wait(150000);
            }

            transportWeb.DeliverAsync(myMessage).Wait(350000);


        }

        protected Cruise AddStaysToCruise (IList<double> staysCoord, IList<double> curves, IList<double> altStaysCoord, int CruiseId)
        {
            var user = HttpContext.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindById(User.Identity.GetUserId());
            Cruise Cruise = new Cruise();
            Cruise.CruiseId = CruiseId;
            Cruise.Stays = new List<Stay>();

            for (int i = 0; i<staysCoord.Count; i += 2)
            {
                Stay stay = new Stay();
                int ii = i + 1;
                stay.Longitude = staysCoord[ii];
                stay.Latitude = staysCoord[i];
                stay.IsAlternative = false;
                stay.StayNumber = Cruise.Stays.Count+1;
                stay.AccountId = user.AccountID; 
                stay.CruiseId = CruiseId;
                Cruise.Stays.Add(stay);
            }
            
            for (int i =0; i<curves.Count; i++)
            {
                Cruise.Stays[i].LegCurvature = (float)curves[i];
            }
            if (altStaysCoord == null)
                return Cruise;
            for (int i = 0; i < altStaysCoord.Count; i += 2)
            {
                Stay stay = new Stay();
                int ii = i + 1;
                stay.Longitude = altStaysCoord[ii];
                stay.Latitude = altStaysCoord[i];
                stay.IsAlternative = true;
                stay.StayNumber = 0;
                stay.AccountId = user.AccountID;
                stay.CruiseId = CruiseId;
                Cruise.Stays.Add(stay);
            }
            return Cruise;
        }


        [HttpGet]
        [Authorize]
        public ActionResult DeleteCruise(int id)
        {
            var user = HttpContext.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindById(User.Identity.GetUserId());
            //Cruise cruise = CruiseService.GetCruiseById(id); // maybe get AccountId from HTML?????

            //if (cruise.OwnerAccountId != user.AccountID)
            //    return null;

            CruiseService.DeleteCruise(id);

            return RedirectToAction("MyCruises", "Home");

        }

        [HttpPost]
        public ActionResult Search(string search)
        {
            var bingContainer = new BingSearchContainer(new Uri("https://api.datamarket.azure.com/Bing/SearchWeb/"));
            var accountKey = ConfigurationManager.AppSettings.Get("bingkey");
            bingContainer.Credentials = new NetworkCredential(accountKey, accountKey);
            var mysite = ConfigurationManager.AppSettings.Get("mysite");
            var webResults = bingContainer.Web(mysite + search, null, null, null, null, null, null, null);
            
            SearchModel searchModel = new SearchModel();
           searchModel.webResults = webResults.Execute();
            return View(searchModel);
        }
    }
}