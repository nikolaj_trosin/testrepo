﻿using Domain.Interfaces;
using Domain.SubDomain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DataContract]
    public class Cruise: DomainBase, IAccountOwnedEntity, IIdentifiableEntity
    {
        [DataMember]
        [MaxLength(200)]
        [Required]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(3000)]
        [Required]
        public string Description { get; set; }


        [DataMember]
        public int CruiseId { get; set; }

        [DataMember]
        public int AccountId { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string VesselName { get; set; }

        [DataMember]
        [MaxLength(2500)]
        [Required]
        public string VesselDescription { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string CapName { get; set; }

        [DataMember]
        [MaxLength(2500)]
        [Required]
        public string CapDescription { get; set; }

        [DataMember]
        [Range(0, 1000)]
        [Required]
        public int VacantPlaces { get; set; }

        [DataMember]
        [MaxLength(50)]
        [Required]
        public string Price { get; set; }

        //public Vessel Vessel { get; set; }
        //[DataMember]
        //public List<Comment> Comments { get; set; }
        //[DataMember]
        //public List<SubDomain.Task> Tasks { get; set; }
        //[DataMember]
        //public List<Note> Notes { get; set; }
        [DataMember]
        public List<Photo> Photos { get; set; }

        [DataMember]
        public List<Stay> Stays { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; } // maybe add DateTime range as property here

        [DataMember]
        [Required]
        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }

        //public string AddPhoto()
        //{
        //    Photo photo = new Photo();
        //    photo.FileName = Path.GetRandomFileName()+".jpg";
        //    photo.CreationTime = DateTime.Now;
        //    if (Photos == null)
        //        Photos = new List<Photo>();
        //    Photos.Add(photo);
        //    return photo.FileName;
        //}
        public void LoadPhotos(string UploadFolder)
        {
            // Photos.ForEach(p => p.PhotoImage = System.Drawing.Image.FromFile("d://Static//1//" + p.FileName, true));
            if(Photos!=null && Photos.Count > 0)
            {
                foreach (var image in Photos)
                {
                    var image_file = System.Drawing.Image.FromFile(UploadFolder+CruiseId.ToString()+"\\" + image.FileName, true);
                    MemoryStream ms = new MemoryStream();
                    image_file.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] imgBytes = ms.ToArray();
                    string string_image = Convert.ToBase64String(imgBytes);
                    image.PhotoImage = "data:image/png;base64,"+ string_image;
                }
            }

        }

        public void CheckIfPhotoExist()
        {
            if (Photos != null && Photos.Count == 0)
            {
                Photo photo = new Photo();
               // photo.CruiseId = this.CruiseId;
                photo.FileName = "zont.jpg";
                //  photo.Path = "//";
                photo.IsMain = true;
                Photos.Add(photo);
            }
            if (Photos == null)
            {
                Photos = new List<Photo>();
                Photo photo = new Photo();
                photo.Name = "/zont.jpg";
                photo.IsMain = true;
                Photos.Add(photo);
            }

        }
        public int EntityId
        {
            get
            {
                return CruiseId;
            }

            set
            {
                CruiseId = value;
            }
        }

        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }

        public static void DeleteFolder(string path)
        {
            if(Directory.Exists(path))
                Directory.Delete(path, true);
        }

        public string DescriptionCut()
        {
            if (String.IsNullOrEmpty(this.Description))
            { return String.Empty; }
            int length = this.Description.Length;
            string dots = " . . . ";
            if (length > 250)
            { return this.Description.Substring(0, 249)+ dots; }
            else
            { return this.Description; }

        }
    }
}
