﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.SubDomain
{
    public class Task: SubDomainBase, IIdentifiableEntity, IAccountOwnedEntity
    {

        public int AccountId { get; set; }

        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
        public int EntityId
        {
            get
            {
                return TaskId;
            }

            set
            {
                TaskId = value;
            }
        }

      //  [DataMember]
        public int TaskId { get; set; }
        public bool IsDone { get; set; }


    }
}
