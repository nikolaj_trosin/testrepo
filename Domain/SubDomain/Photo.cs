﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace Domain.SubDomain
{
   // [DataContract]
    public class Photo: SubDomainBase, IIdentifiableEntity, IAccountOwnedEntity
    {
        [DataMember]
        public int AccountId { get; set; }

        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
        public int EntityId
        {
            get
            {
                return PhotoId;
            }

            set
            {
                PhotoId = value;
            }
        }

        [DataMember]
        public bool IsMain { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string FileName { get; set; }
        [DataMember]
        public int PhotoId { get; set; }
        [DataMember]
        public string PhotoImage { get; set; }
        [DataMember]
        public int CruiseId { get; set; }

        public string Path { get {  if (CruiseId != 0) { return CruiseId.ToString() + "//" + FileName; } else { return FileName; } }
        }

        public bool Delete(string uploadFolder)
        {
            if (File.Exists(uploadFolder+this.Path))
            {
                File.Delete(uploadFolder + this.Path);
                return true;
            }
            return false;
        }
    }
}
