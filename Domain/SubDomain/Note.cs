﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.SubDomain
{
    public class Note: SubDomainBase, IIdentifiableEntity, IAccountOwnedEntity
    {
        public int AccountId { get; set; }
       // [DataMember]
        public int NoteId { get; set; }

        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
        public int EntityId
        {
            get
            {
                return NoteId;
            }

            set
            {
                NoteId = value;
            }
        }
    }
}
