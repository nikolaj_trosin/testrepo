﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.SubDomain
{
    public class SubDomainBase: IExtensibleDataObject
    {
        public static CompositionContainer Container { get; set; }
        public ExtensionDataObject ExtensionData { get; set; }

        [DataMember]
        [MaxLength(200)]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(2500)]
        public string Description { get; set; }

    }
}
