﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.SubDomain
{
    public class Comment: SubDomainBase, IIdentifiableEntity, IAccountOwnedEntity
    {
        public int AccountId { get; set; }
      //  [DataMember]
        public int CommentId { get; set; }
        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
        public int EntityId
        {
            get
            {
                return CommentId;
            }

            set
            {
                CommentId = value;
            }
        }

    }
}
