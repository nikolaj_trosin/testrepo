﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using Domain.SubDomain;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    [DataContract]
    public abstract class DomainBase: IExtensibleDataObject
    {
        public static CompositionContainer Container { get; set; }
        public ExtensionDataObject ExtensionData { get; set; }                            
    }
}
