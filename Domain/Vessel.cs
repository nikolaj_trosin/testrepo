﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Domain
{
    public class Vessel: DomainBase, IAccountOwnedEntity, IIdentifiableEntity
    {

        [DataMember]
        [MaxLength(200)]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(3000)]
        public string Description { get; set; }

        public int VesselId { get; set; }
        public int EntityId
        {
            get
            {
                return VesselId;
            }

            set
            {
                VesselId=value;
            }
        }

        public string Length { get; set; }
        public int NumberOfPlaces { get; set; }
        public int AccountId { get; set; }


        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
    }
}
