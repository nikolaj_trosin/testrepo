﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
   public class Stay: DomainBase, IAccountOwnedEntity, IIdentifiableEntity
    {
        [DataMember]
        [MaxLength(200)]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(3000)]
        public string Description { get; set; }

        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public int AccountId { get; set; }
        [DataMember]
        public int StayId { get; set; }
        [DataMember]
        public int StayNumber { get; set; }
        // public List<Stay> AlternativeStays { get; set; }
        [DataMember]
        public float LegCurvature { get; set; }
        [DataMember]
        public int CruiseId { get; set; }
        [DataMember]
        public bool IsAlternative { get; set; } = false;

    public int EntityId
        {
            get
            {
                return StayId;
            }

            set
            {
                StayId = value;
            }
        }

        int IAccountOwnedEntity.OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
    }
}
