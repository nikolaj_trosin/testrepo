﻿using Domain.Interfaces;
using System.Runtime.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain
{

    [DataContract]
    public class Account: IIdentifiableEntity, IAccountOwnedEntity
    {
        public int OwnerAccountId
        {
            get
            {
                return AccountId;
            }
        }
        public int EntityId
        {
            get
            {
                return AccountId;
            }

            set
            {
                AccountId = value;
            }
        }

        [DataMember]
        public int AccountId { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string Name { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string LoginEmail { get; set; }

    }
}
