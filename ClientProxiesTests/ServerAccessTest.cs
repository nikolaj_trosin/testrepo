﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Proxies;
using RoutePlannerServices;

namespace ClientProxiesTests
{
    [TestClass]
    public class ServerAccessTest
    {
        [TestMethod]
        public void Test_Access_Client_connection_to_Service()
        {
            AccountClientProxy proxy = new AccountClientProxy();
            proxy.Open();
        }


        public void Test_Access_Client_connection_to_CruiseService()
        {
            CruiseProxy proxy = new CruiseProxy();
            proxy.Open();
        }
    }
}
