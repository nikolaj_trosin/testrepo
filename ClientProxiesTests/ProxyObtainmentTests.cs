﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain;
using ClientBootstrapper;
using RoutePlannerServices;
using Proxies;
using Contracts; 

namespace ClientProxiesTests
{
    [TestClass]
    public class ProxyObtainmentTests
    {
        [TestInitialize]
        public void Initialize()
        {
            DomainBase.Container = ClientBootstrapper.MEFLoader.Init(); 
        }

        [TestMethod]
        public void obtain_proxy_from_container_using_service_contract()
        {
            IAccountContract accountContractProxy = DomainBase.Container.GetExportedValue<IAccountContract>();
            ICruiseContract CruiseProxy = DomainBase.Container.GetExportedValue<ICruiseContract>();

            Assert.IsTrue(CruiseProxy is CruiseProxy);
            Assert.IsTrue(accountContractProxy is AccountClientProxy);
        }

        [TestMethod]
        public void obtain_proxy_from_service_factory()
        {
            IServiceProxyFactory proxyFactory = new ServiceProxyFactory();
            IAccountContract accountContractProxy = proxyFactory.CreateServiceProxy<IAccountContract>();
            ICruiseContract CruiseProxy = proxyFactory.CreateServiceProxy<ICruiseContract>();

            Assert.IsTrue(CruiseProxy is CruiseProxy);
            Assert.IsTrue(accountContractProxy is AccountClientProxy);
        }

        [TestMethod]
        public void obtain_proxy_and_factory_from_container()
        {
            IServiceProxyFactory proxyFactory = DomainBase.Container.GetExportedValue<IServiceProxyFactory>();
            IAccountContract accountContractProxy = proxyFactory.CreateServiceProxy<IAccountContract>();
            ICruiseContract CruiseProxy = proxyFactory.CreateServiceProxy<ICruiseContract>();

            Assert.IsTrue(CruiseProxy is CruiseProxy);
            Assert.IsTrue(accountContractProxy is AccountClientProxy);
        }

    }
}
