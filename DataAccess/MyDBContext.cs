﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Domain;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain.Interfaces;
using Domain.SubDomain;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    public class MyDBContext : DbContext
    {
        public MyDBContext() : base("name=MyConnectionString") // connection string name passed to base constractor
        {
          //   Database.SetInitializer<MyDBContext>(null); // 039. Creating the DB-Context 
            Database.SetInitializer<MyDBContext>(new CreateDatabaseIfNotExists<MyDBContext>());
        }


        public DbSet<Account> AccountSet { get; set; }
        public DbSet<Cruise> CruiseSet { get; set; }
      
        public DbSet<Stay> StaySet { get; set; }
        //  public DbSet<Note> NoteSet { get; set; }
        //   public DbSet<Domain.SubDomain.Task> TaskSet { get; set; }
        //  public DbSet<Comment> CommentSet { get; set; }
        public DbSet<Photo> PhotoSet { get; set; }
       // public DbSet<Vessel> VesselSet { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        modelBuilder.Ignore<IIdentifiableEntity>();
        modelBuilder.Ignore<IExtensibleDataObject>();

            
            modelBuilder.Entity<Cruise>()
            .HasMany(p => p.Stays)
            .WithOptional()
            .WillCascadeOnDelete(true);
            modelBuilder.Entity<Cruise>()
            .HasMany(p => p.Photos)
            .WithOptional()
            .WillCascadeOnDelete(true);


            modelBuilder.Entity<Cruise>().Ignore(f=>f.ExtensionData);
            modelBuilder.Entity<Stay>().Ignore(f => f.ExtensionData);
            //modelBuilder.Entity<Note>().Ignore(f => f.ExtensionData);
            //modelBuilder.Entity<Comment>().Ignore(f => f.ExtensionData);
            modelBuilder.Entity<Photo>().Ignore(f => f.ExtensionData);
            modelBuilder.Entity<Photo>().Ignore(f => f.PhotoImage);

            //modelBuilder.Entity<Domain.SubDomain.Task>().Ignore(f => f.ExtensionData);
            //modelBuilder.Entity<Vessel>().Ignore(f => f.ExtensionData);


        modelBuilder.Entity<Cruise>().HasKey<int>(a => a.CruiseId).Ignore(e => e.EntityId);
        modelBuilder.Entity<Account>().HasKey<int>(a => a.AccountId).Ignore(e => e.EntityId);
        modelBuilder.Entity<Stay>().HasKey<int>(a => a.StayId).Ignore(e => e.EntityId);
        //modelBuilder.Entity<Note>().HasKey<int>(a => a.NoteId).Ignore(e => e.EntityId);
        //modelBuilder.Entity<Comment>().HasKey<int>(a => a.CommentId).Ignore(e => e.EntityId);
        modelBuilder.Entity<Photo>().HasKey<int>(a => a.PhotoId).Ignore(e => e.EntityId);
            modelBuilder.Entity<Stay>().Property(p => p.LegCurvature).IsOptional();
            //modelBuilder.Entity<Domain.SubDomain.Task>().HasKey<int>(a => a.TaskId).Ignore(e => e.EntityId);
            //modelBuilder.Entity<Vessel>().HasKey<int>(a => a.VesselId).Ignore(e => e.EntityId);

            
        }


    }
}

