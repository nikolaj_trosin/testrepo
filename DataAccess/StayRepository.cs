﻿using DataAccess.Interfaces;
using Domain;

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    [Export(typeof(IStayRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class StayRepository : DataRepositoryBase<Stay, MyDBContext>, IStayRepository
    {
        protected override void AddEntities(MyDBContext entityContext, IEnumerable<Stay> entitySet, int id)
        {
            foreach(var stay in entitySet)
            {
                stay.CruiseId = id;
            }
            entityContext.StaySet.AddRange(entitySet);
        }

        protected override Stay AddEntity(MyDBContext entityContext, Stay entity)
        {
            throw new NotImplementedException();
        }

        protected override void DeleteEntities(MyDBContext entityContext, int id)
        {
            entityContext.StaySet.RemoveRange(entityContext.StaySet.Where(s=>s.CruiseId==id));
        }

        protected override int GetAllEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Stay> GetAllEntities(MyDBContext entityContext, int PageNumber)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Stay> GetAllEntitiesByCruiseId(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Stay> GetEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Stay> GetEntities(MyDBContext entityContext, int AccountId)
        {
            throw new NotImplementedException();
        }

        protected override Stay GetEntity(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override Stay UpdateEntity(MyDBContext entityContext, Stay entity)
        {
            throw new NotImplementedException();
        }


        
    }
}
