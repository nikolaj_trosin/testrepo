﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;

namespace DataAccess
{
    public interface IDataRepository
    {}

    public interface IDataRepository<T> : IDataRepository
         where T : class, IIdentifiableEntity, new()
    {
        T Add(T entity);

        void Remove(T entity);

        void Remove(int id);

        T Update(T entity);

        IEnumerable<T> GetAll(int CurrentPage);

        IEnumerable<T> Get();

        int GetAll();

        T Get(int id);

        void DeleteAllEntitiesByCruiseId(int id);

        IEnumerable<T> GetAllEntitiesByCruiseId(int id);

        void AddEntitiesWithCruiseId(IEnumerable<T> entitySet, int id);

        IEnumerable<T> GetByAccountId(int AccountId);
        
    }
}
