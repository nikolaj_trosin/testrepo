﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
   public interface IAccountRepository: IDataRepository<Account>
    {
        Account GetByLogin(string login);

    }
}
