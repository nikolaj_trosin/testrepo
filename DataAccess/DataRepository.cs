﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interfaces;
using System.Data.Entity;

namespace DataAccess
{
    public abstract class DataRepositoryBase<T,U>: IDataRepository<T>
                         where T : class, IIdentifiableEntity, new()
                         where U : DbContext, new()

    {
        protected abstract T AddEntity(U entityContext, T entity);

        protected abstract T UpdateEntity(U entityContext, T entity);

        protected abstract IEnumerable<T> GetEntities(U entityContext);

        protected abstract IEnumerable<T> GetEntities(U entityContext, int AccountId);

        protected abstract IEnumerable<T> GetAllEntities(U entityContext, int PageNumber);

        protected abstract int GetAllEntities(U entityContext);


        protected abstract T GetEntity(U entityContext, int id);

        protected abstract void DeleteEntities(U entityContext, int id);

        protected abstract IEnumerable<T> GetAllEntitiesByCruiseId(U entityContext, int id);


        protected abstract void AddEntities(U entityContext, IEnumerable<T> entitySet, int id);


        public T Add(T entity)
        {
            using (U entityContext = new U())
            {
                T addedEntity = AddEntity(entityContext, entity);
                entityContext.SaveChanges();
                return addedEntity;
            }
        }

        public void Remove(T entity)
        {
            using (U entityContext = new U())
            {
                entityContext.Entry<T>(entity).State = EntityState.Deleted;
                entityContext.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (U entityContext = new U())
            {
                T entity = GetEntity(entityContext, id);
                entityContext.Entry<T>(entity).State = EntityState.Deleted;
                entityContext.SaveChanges();
            }
        }

        public T Update(T entity)
        {
            using (U entityContext = new U())
            {
                T existingEntity = UpdateEntity(entityContext, entity);

                SimpleMapper.PropertyMap(entity, existingEntity);

                entityContext.SaveChanges();
                return existingEntity;
            }
        }

        public IEnumerable<T> Get()
        {
            using (U entityContext = new U())
                return (GetEntities(entityContext)).ToArray().ToList();
        }

        public IEnumerable<T> GetByAccountId(int AccountId)
        {
            using (U entityContext = new U())
                return (GetEntities(entityContext, AccountId)).ToArray().ToList();
        }

        public void DeleteAllEntitiesByCruiseId(int id)
        {
            using (U entityContext = new U())
            {
                DeleteEntities(entityContext, id);
                entityContext.SaveChanges();
            }
        }

        public void AddEntitiesWithCruiseId(IEnumerable<T> entitySet, int id)
        {
            using (U entityContext = new U())
            {
                AddEntities(entityContext, entitySet, id);
                entityContext.SaveChanges();
            }
        }
        public T Get(int id)
        {
            using (U entityContext = new U())
                return GetEntity(entityContext, id);
        }

        public IEnumerable<T> GetAllEntitiesByCruiseId(int id)
        {
            using (U entityContext = new U())
                return (GetAllEntitiesByCruiseId(entityContext, id)).ToArray().ToList();
        }

        public IEnumerable<T> GetAll(int CurrentPage)
        {
            using (U entityContext = new U())
                return (GetAllEntities(entityContext, CurrentPage)).ToArray().ToList();
        }

        public int GetAll()
        {
            using (U entityContext = new U())
                return GetAllEntities(entityContext);
        }
    }
}
