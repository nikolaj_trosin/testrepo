﻿using DataAccess.Interfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    [Export(typeof(IAccountRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AccountRepository : DataRepositoryBase<Account, MyDBContext>, IAccountRepository
    {
        protected override Account AddEntity(MyDBContext entityContext, Account entity)
        {
            return entityContext.AccountSet.Add(entity);
        }

        protected override IEnumerable<Account> GetEntities(MyDBContext entityContext)
        {
            return (from e in entityContext.AccountSet select e);
        }

        protected override Account GetEntity(MyDBContext entityContext, int id)
        {
            var query = from e in entityContext.AccountSet
                        where e.AccountId == id
                        select e;
            var results = query.FirstOrDefault();

            return results;
        }

        protected override Account UpdateEntity(MyDBContext entityContext, Account entity)
        {
            return (from e in  entityContext.AccountSet
                    where e.AccountId==entity.AccountId
                    select e ).FirstOrDefault();
        }


        public Account GetByLogin(string login)
        {
            using (MyDBContext entityContext = new MyDBContext())
            {
                return (from e in entityContext.AccountSet
                        where e.LoginEmail == login
                        select e).FirstOrDefault();
            }

        }

        protected override void DeleteEntities(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override void AddEntities(MyDBContext entityContext, IEnumerable<Account> entitySet, int id)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Account> GetEntities(MyDBContext entityContext, int AccountId)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Account> GetAllEntitiesByCruiseId(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Account> GetAllEntities(MyDBContext entityContext, int PageNumber)
        {
            throw new NotImplementedException();
        }

        protected override int GetAllEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }
    }
}
