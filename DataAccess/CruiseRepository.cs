﻿using DataAccess.Interfaces;
using Domain;

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    [Export(typeof(ICruiseRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CruiseRepository : DataRepositoryBase<Cruise, MyDBContext>, ICruiseRepository
    {
        protected override void AddEntities(MyDBContext entityContext, IEnumerable<Cruise> entitySet, int id)
        {
            throw new NotImplementedException();
        }

        protected override Cruise AddEntity(MyDBContext entityContext, Cruise entity)
        {
            return entityContext.CruiseSet.Add(entity);
        }

        protected override void DeleteEntities(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override int GetAllEntities(MyDBContext entityContext)
        {
           return entityContext.CruiseSet.Include("Photos").Include("Stays").Where(c => c.Photos.Count > 2 && c.Stays.Count > 1).Count(); 
        }

        protected override IEnumerable<Cruise> GetAllEntities(MyDBContext entityContext, int CurrentPage)
        {
            return entityContext.CruiseSet.Include("Photos").Include("Stays").Where(c=>c.Photos.Count>2 && c.Stays.Count>1).OrderBy(c => c.CruiseId).Skip((CurrentPage - 1)*5).Take(5);
        }

        protected override IEnumerable<Cruise> GetAllEntitiesByCruiseId(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Cruise> GetEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Cruise> GetEntities(MyDBContext entityContext, int AccountId)
        {
            var query = from e in entityContext.CruiseSet.Include("Photos")
                        where e.AccountId == AccountId
                        select e;
            return query;
        }

        protected override Cruise GetEntity(MyDBContext entityContext, int id)
        {
            var query = from e in entityContext.CruiseSet.Include("Photos").Include("Stays")
                        where e.CruiseId == id
                        select e;
            var results = query.FirstOrDefault();

            return results;
        }

        protected override Cruise UpdateEntity(MyDBContext entityContext, Cruise entity)
        {
            return (from e in entityContext.CruiseSet
                    where e.CruiseId == entity.CruiseId
                    select e).FirstOrDefault();
        }
    }
}
