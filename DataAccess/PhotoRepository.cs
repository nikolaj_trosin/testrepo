﻿using Domain.SubDomain;
using System;
using System.Collections.Generic;
using DataAccess.Interfaces;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    [Export(typeof(IPhotoRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PhotoRepository : DataRepositoryBase<Photo, MyDBContext>, IPhotoRepository
    {
        protected override void AddEntities(MyDBContext entityContext, IEnumerable<Photo> entitySet, int id)
        {
            throw new NotImplementedException();
        }


        protected override Photo AddEntity(MyDBContext entityContext, Photo entity)
        {
            return entityContext.PhotoSet.Add(entity);
        }

        protected override void DeleteEntities(MyDBContext entityContext, int id)
        {
            entityContext.PhotoSet.RemoveRange(entityContext.PhotoSet.Where(s => s.CruiseId == id));
        }

        protected override int GetAllEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Photo> GetAllEntities(MyDBContext entityContext, int PageNumber)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Photo> GetAllEntitiesByCruiseId(MyDBContext entityContext, int id)
        {
            return entityContext.PhotoSet.Where(p => p.CruiseId == id);
        }

        protected override IEnumerable<Photo> GetEntities(MyDBContext entityContext)
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<Photo> GetEntities(MyDBContext entityContext, int AccountId)
        {
            throw new NotImplementedException();
        }

        protected override Photo GetEntity(MyDBContext entityContext, int id)
        {
            throw new NotImplementedException();
        }

        protected override Photo UpdateEntity(MyDBContext entityContext, Photo entity)
        {
            return (from e in entityContext.PhotoSet
                    where e.PhotoId == entity.PhotoId
                    select e).FirstOrDefault();
        }
    }
}
