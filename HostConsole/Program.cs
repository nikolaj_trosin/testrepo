﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Threading.Tasks;
using RoutePlannerServices;
using System.ServiceModel;
using System.Security.Principal;
using System.Timers;
using Domain;
using System.Threading;
using Bootstrapper;
using System.ComponentModel.Composition;
using System.Data.SqlClient;


namespace HostConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DomainBase.Container = MefLoader.Init();

            Console.WriteLine("Starting up....");
            Console.WriteLine("");
            // can start more than one host 
        //    System.ServiceModel.ServiceHost AccountManagerHost = new System.ServiceModel.ServiceHost(typeof(AccountManager));
            System.ServiceModel.ServiceHost CruiseManagerHost = new System.ServiceModel.ServiceHost(typeof(CruiseManager));
            System.ServiceModel.ServiceHost FileManagerHost = new System.ServiceModel.ServiceHost(typeof(FileManager));
            ServiceHost PictureService = new ServiceHost(typeof(WwwServiceImplementation));

            StartService(CruiseManagerHost, "Cruise Manager");
        //    StartService(AccountManagerHost, "Account Manager");
            StartService(FileManagerHost, "File Manager");
            StartService(PictureService, "WWW PictureService");

            //System.Timers.Timer timer = new System.Timers.Timer(10000);
            //timer.Elapsed += onTimerElapsed; 


            // timer.Start();
            // Console.WriteLine("Timer started...");


            Console.WriteLine("");
            Console.WriteLine("Press [Enter] to exit");
            Console.ReadLine();
            //timer.Stop();
            StopService(CruiseManagerHost, "Cruise Manager");
            StopService(FileManagerHost, "File Manager");
            StopService(PictureService, "Picture Manager");


        }

        static void onTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Adding new Account at: {0}", DateTime.Now);
            AccountManager accountManager = new AccountManager();
            Account acc = new Account() { Name="New Freakin' Cap"};
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    accountManager.UpdateAccount(acc);
                    scope.Complete();
                    Console.WriteLine("new Account was added at: {0}", DateTime.Now);
                }
                catch (Exception ex) { Console.WriteLine(String.Format("Exception : {0}", ex.Message)); }
            }
        }

        static void StartService(System.ServiceModel.ServiceHost host, string serviceDescription)
        {
            host.Open();
            Console.WriteLine("Service {0} started", serviceDescription);

            foreach (var endpoint in host.Description.Endpoints)
            {
                Console.WriteLine(String.Format("Listening to endpoint:"));
                Console.WriteLine(String.Format("Adress: {0}",endpoint.Address.Uri));
                Console.WriteLine(String.Format("Binding: {0}", endpoint.Binding.Name));
                Console.WriteLine(String.Format("Contract: {0}", endpoint.Contract.Name));
                Console.WriteLine(String.Format("Contract: {0}", endpoint.Contract.ConfigurationName));
            }
            Console.WriteLine();
        }


        static void StopService(System.ServiceModel.ServiceHost host, string serviceDescription)
        {
            host.Close();
            Console.WriteLine("Service {0} stopped", serviceDescription);

        }
    }
}
