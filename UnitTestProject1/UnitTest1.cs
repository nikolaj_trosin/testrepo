﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition;
using DataAccess.Interfaces;
using Bootstrapper;
using Domain;
using System.Collections.Generic;
using Moq;
using System.Data.SqlClient; 

namespace UnitTestProject1
{
    [TestClass]
    public class DataLayerTest
    {


        [TestInitialize]
        public void Initialize()
        {
            DomainBase.Container = MefLoader.Init();
        }

        [TestMethod]
        public void Test_Repository_usage()
        {
            RepositoryTestClass repoTest = new RepositoryTestClass();
            IEnumerable<Account> accounts = repoTest.GetAccounts();
            Assert.IsTrue(accounts != null);
        }

        [TestMethod]
        public void Test_Cruise_repository()
        {
            RepositoryFactoryTestClass repoFactory_Test = new RepositoryFactoryTestClass();
            IEnumerable<Cruise> Cruises = repoFactory_Test.GetCruises();

            Assert.IsTrue(Cruises != null);
        }

        [TestMethod]
        public void Test_Repository_Factory_usage()
        {
            RepositoryFactoryTestClass repoFactory_Test = new RepositoryFactoryTestClass();
            IEnumerable<Account> accounts = repoFactory_Test.GetAccounts();

            Assert.IsTrue(accounts != null);
        }


        [TestMethod]
        public void Test_Repository_Factory_Mocking()
        {
            List<Account> accounts = new List<Account>()
            {
                new Account() { Name ="Caap", LoginEmail="cam@volk.net"},
                new Account() { Name = "Boss", LoginEmail = "Boss@volk.net" }
        };
            Mock<IAccountRepository> mockAccountRepo = new Mock<IAccountRepository>();
            mockAccountRepo.Setup(obj => obj.Get()).Returns(accounts);

            Mock<IDataRepositoryFactory> mockAccountRepoFactory = new Mock<IDataRepositoryFactory>();
            mockAccountRepoFactory.Setup(obj => obj.GetDataRepository<IAccountRepository>()).Returns(mockAccountRepo.Object);

            RepositoryFactoryTestClass repoFactory_Test = new RepositoryFactoryTestClass(mockAccountRepoFactory.Object);
            IEnumerable<Account> _accounts = repoFactory_Test.GetAccounts();

            Assert.IsTrue(_accounts != null);
        }

        [TestMethod]
        public void Test_Repository_Mocking()
        {
            List<Account> accounts = new List<Account>()
            {
                new Account() { Name ="Caap", LoginEmail="cam@volk.net"},
                new Account() { Name = "Boss", LoginEmail = "Boss@volk.net" }
        };
            Mock<IAccountRepository> mockAccountRepo = new Mock<IAccountRepository>();
            mockAccountRepo.Setup(obj=>obj.Get()).Returns(accounts);

            RepositoryTestClass repoTest = new RepositoryTestClass(mockAccountRepo.Object);
            var accounts_ = repoTest.GetAccounts();
            Assert.IsTrue(accounts==accounts_);

        }
    }


    public class RepositoryTestClass
    {
        [Import]
        IAccountRepository AccountRepository; // Old implementation without Abstract factory pattern

        [Import]
        ICruiseRepository CruiseRepository;

        public RepositoryTestClass()
        {
            DomainBase.Container.SatisfyImportsOnce(this);

        }

        public RepositoryTestClass(IAccountRepository accountRepository)
        {
            AccountRepository = accountRepository;
        }

       

        public IEnumerable<Account> GetAccounts()
        {
            IEnumerable<Account> accounts = AccountRepository.Get();
            return accounts;

        }
    }

    public class RepositoryFactoryTestClass
    {
        public RepositoryFactoryTestClass()
        {
            DomainBase.Container.SatisfyImportsOnce(this);
        }

        public RepositoryFactoryTestClass(IDataRepositoryFactory _dataRepositoryFactory)
        {
            DataRepositoryFactory = _dataRepositoryFactory;
        }

        [Import]
        IDataRepositoryFactory DataRepositoryFactory; 

        public IEnumerable<Account> GetAccounts()
        {
            IAccountRepository AccountRepository = DataRepositoryFactory.GetDataRepository<IAccountRepository>();
            IEnumerable<Account> accounts = AccountRepository.Get();

            return accounts;
        }


        public IEnumerable<Cruise> GetCruises()
        {
            ICruiseRepository CruiseRepository = DataRepositoryFactory.GetDataRepository<ICruiseRepository>();
            IEnumerable<Cruise> Cruises = CruiseRepository.Get();

            return Cruises;
        }
    }
}
