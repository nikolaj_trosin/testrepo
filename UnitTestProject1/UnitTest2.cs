﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel.Composition;
using DataAccess.Interfaces;
using Bootstrapper;
using Domain;
using System.Security.Principal;
using Moq;
using RoutePlannerServices;
using System.Threading;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest2
    {

        [TestInitialize]
        public void Initialize()
        {
            GenericPrincipal principal = new GenericPrincipal(new GenericIdentity("Nick"), new string[] { "HomeUsers" } );
            Thread.CurrentPrincipal = principal;
        }

        [TestMethod]
        public void UpdateAccount()
        {
            Account New_Acc = new Account();
            Account Added_Acc = new Account() { AccountId=1 };

            Mock<IDataRepositoryFactory> mockRepoFactory = new Mock<IDataRepositoryFactory>();
            mockRepoFactory.Setup(obj => obj.GetDataRepository<IAccountRepository>().Add(New_Acc)).Returns(Added_Acc);

            AccountManager AccountManager = new AccountManager(mockRepoFactory.Object);

            Account result = AccountManager.UpdateAccount(New_Acc);
            Assert.IsTrue(result== Added_Acc);
        }

        [TestMethod]
        public void Add_new_Account()
        {
            Account Existing_Acc = new Account() { AccountId = 1 };
            Account Updated_Acc = new Account() { AccountId = 1 };

            Mock<IDataRepositoryFactory> mockRepoFactory = new Mock<IDataRepositoryFactory>();
            mockRepoFactory.Setup(obj => obj.GetDataRepository<IAccountRepository>().Update(Existing_Acc)).Returns(Updated_Acc);

            AccountManager AccountManager = new AccountManager(mockRepoFactory.Object);

            Account result = AccountManager.UpdateAccount(Existing_Acc);
            Assert.IsTrue(result == Updated_Acc);
        }
    }
}
