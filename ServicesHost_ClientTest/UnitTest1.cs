﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel;
using RoutePlannerServices;
using Contracts;
namespace ServicesHost_ClientTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_AccountManager_as_Service() // not careating proxy in normal way  - 097
        {
            ChannelFactory<IAccountContract> channelFactory = new ChannelFactory<IAccountContract>("");
            IAccountContract proxy = channelFactory.CreateChannel();
            (proxy as ICommunicationObject).Open();

            channelFactory.Close();
        }
        [TestMethod]
        public void Test_CruiseManager_as_Service() // not careating proxy in normal way  - 097
        {
            ChannelFactory<ICruiseContract> channelFactory = new ChannelFactory<ICruiseContract>("");
            ICruiseContract proxy = channelFactory.CreateChannel();
            (proxy as ICommunicationObject).Open();

            channelFactory.Close();
        }
    }
}
