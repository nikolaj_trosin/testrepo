﻿using System;
using Contracts;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceModel;

namespace Proxies
{
    [Export(typeof(IFileContract))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FileProxy : UserClientBase<IFileContract>, IFileContract
    {
        public void GetFile(FileUploadMessage file)
        {
             AddHeader();
             Channel.GetFile(file);
        }
    }
}
