﻿using RoutePlannerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.ServiceModel;
using System.ComponentModel.Composition;
using System.ServiceModel.Channels;
using Contracts;



namespace Proxies
{
    [Export(typeof(IAccountContract))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AccountClientProxy :  UserClientBase<IAccountContract>, IAccountContract
    {
        
        public void DeleteAccount(int id)
        {
            Channel.DeleteAccount(id);
        }

        public Account GetAccount(string login)
        {
           return Channel.GetAccount(login); 
        }

        public Account GetAccountById(int id)
        {
            return Channel.GetAccountById(id);
        }

        public Account[] GetAllAccounts()
        {
            return Channel.GetAllAccounts();
        }

        public Account UpdateAccount(Account account)
        {
            return Channel.UpdateAccount(account);
        }
    }
}
