﻿using RoutePlannerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.ServiceModel;
using System.ComponentModel.Composition;
using System.ServiceModel.Channels;
using Contracts;
using System.IO;

namespace Proxies
{
    
    class WwwServiceProx : UserClientBase<IWwwService>, IWwwService
    {
        public Stream StaticContent(string content)
        {
            return Channel.StaticContent(content);
        }
    }
}
