﻿using Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Domain.SubDomain;
using System.Threading;
using System.ServiceModel;

namespace Proxies
{
    [Export(typeof(ICruiseContract))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CruiseProxy : UserClientBase<ICruiseContract>, ICruiseContract
    {
        //public CruiseProxy(): base()
        //{
            //string username = Thread.CurrentPrincipal.Identity.Name;
            //MessageHeader<string> header = new MessageHeader<string>(username);
            //OperationContextScope contetScope = new OperationContextScope(InnerChannel);
            //OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("String", "System"));
        //}

        public void DeleteCruise(int id)
        {
            AddHeader();
            Channel.DeleteCruise(id);
        }

        public void DeletePhoto(Photo Photo)
        {
            AddHeader();
            Channel.DeletePhoto(Photo);
        }

        public Cruise[] GetAllCruises(int CurrentPage)
        {
            AddHeader();
            return Channel.GetAllCruises(CurrentPage);
        }

        public int GetAllCruisesNumber()
        {
            AddHeader();
            return Channel.GetAllCruisesNumber();
        }

        public Cruise GetCruiseById(int id)
        {
            AddHeader();
            return Channel.GetCruiseById(id);
        }

        public Cruise GetCruises(string login)
        {
            AddHeader();
            throw new NotImplementedException();
        }

        public Cruise[] GetCruisesByAccount(int id)
        {
            AddHeader();
            return Channel.GetCruisesByAccount(id);
        }

        public Cruise GetCruisesByAccount(Account account)
        {
            AddHeader();
            throw new NotImplementedException();
        }

        public void SetMainPhoto(Photo photo)
        {
            AddHeader();
            Channel.SetMainPhoto(photo);
        }

        public Cruise UpdateCruise(Cruise Cruise)
        {
            AddHeader();
            return Channel.UpdateCruise(Cruise);
        }

        public void UpdateStays(Cruise Cruise)
        {
            AddHeader();
            Channel.UpdateStays(Cruise);
        }
    }
}
