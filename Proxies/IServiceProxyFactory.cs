﻿using RoutePlannerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace Proxies
{
   public interface IServiceProxyFactory
    {
        T CreateServiceProxy<T>() where T : IServiceProxyContract;
    }
}
