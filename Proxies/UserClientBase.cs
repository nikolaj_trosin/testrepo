﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel.Channels;
using System.Configuration;

namespace Proxies
{
   public class UserClientBase<T>: ClientBase<T> where T: class
    {
        public UserClientBase()
        {
            string username = Thread.CurrentPrincipal.Identity.Name;
            MessageHeader<string> header = new MessageHeader<string>(username);

            base.ClientCredentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings.Get("name");
            base.ClientCredentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings.Get("pass");

            OperationContextScope contetScope = new OperationContextScope(InnerChannel);
            OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("String", "System"));
        }

        protected void AddHeader()
        {
            string username = Thread.CurrentPrincipal.Identity.Name;
            MessageHeader<string> header = new MessageHeader<string>(username);
            OperationContextScope contetScope = new OperationContextScope(InnerChannel);
            OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader("String", "System"));
        }

    }
}
