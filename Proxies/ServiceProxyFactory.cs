﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoutePlannerServices;
using System.ComponentModel.Composition;
using Contracts;

namespace Proxies
{
    [Export(typeof(IServiceProxyFactory))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
   public class ServiceProxyFactory : IServiceProxyFactory
    {
        public T CreateServiceProxy<T>() where T : IServiceProxyContract
        {
            return Domain.DomainBase.Container.GetExportedValue<T>();
        }
    }
}
