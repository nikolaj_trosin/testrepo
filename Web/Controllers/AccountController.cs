﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
namespace Web.Controllers
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AccountController : ViewControllerBase
    {
        //ISecurityAdapter _SecurityAdapter;

        [ImportingConstructor]
        public AccountController() //ISecurityAdapter securityAdapter
        {
         //   _SecurityAdapter = securityAdapter;
        }


        // GET: Account
        [HttpGet]
        public ActionResult Login(string returnUrl )
        {
           // _SecurityAdapter.Initialize();
            return View(new AccountLoginModel() { ReturnUrl = returnUrl });
        }
    }
}