﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;


namespace ClientBootstrapper
{
    public class MefAPIDependencyResolver: IDependencyScope, IDependencyResolver // : IDependencyScope, IDependencyResolver - added by me
    {
        public MefAPIDependencyResolver(CompositionContainer container)
        {
            _Container = container;
        }

        CompositionContainer _Container;

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public object GetService(Type serviceType)
        {
            return _Container.GetExportedValueByType(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _Container.GetExportedValuesByType(serviceType);
        }

        public void Dispose() // added by me
        {
        }

        System.Web.Http.Dependencies.IDependencyScope IDependencyResolver.BeginScope() // added by me
        {
            return this;
        }
    }
}
