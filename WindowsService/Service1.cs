﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions; // Delete reference ?????
using System.Threading.Tasks;
using RoutePlannerServices;
using System.ServiceModel;
using System.Security.Principal;
using Domain;
using System.Threading;
using Bootstrapper;
using System.ComponentModel.Composition; // Delete reference ?????
using System.Data.SqlClient;
using System.ServiceProcess;


namespace WindowsService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
            
        }

        System.ServiceModel.ServiceHost AccountManagerHost;
        System.ServiceModel.ServiceHost CruiseManager;
        System.ServiceModel.ServiceHost FileManager;
        System.ServiceModel.ServiceHost wwwService;



        protected override void OnStart(string[] args)
        {
            GenericPrincipal principal = new GenericPrincipal(new GenericIdentity("Nick"), new string[] { "HomeUsers" });
            Thread.CurrentPrincipal = principal;
            DomainBase.Container = MefLoader.Init();

            CruiseManager = new System.ServiceModel.ServiceHost(typeof(CruiseManager));
            FileManager = new System.ServiceModel.ServiceHost(typeof(FileManager));
            wwwService = new System.ServiceModel.ServiceHost(typeof(WwwServiceImplementation));

            CruiseManager.Open();
            FileManager.Open();
            wwwService.Open();
        }

        protected override void OnStop()
        {
            CruiseManager.Close();
            FileManager.Close();
            wwwService.Close();
        }
    }



}
