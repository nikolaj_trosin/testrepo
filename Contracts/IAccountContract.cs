﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Domain;
using Contracts;

namespace Contracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAccountContract: IServiceProxyContract // Add namespace to assemblyInfo !!! 059 lesson!!!
    {
        [OperationContract]
        [FaultContract(typeof(NotFoundException))]
        Account GetAccountById(int id);


        [OperationContract]
        [FaultContract(typeof(AuthorizationValidationException))]
        Account GetAccount(string login);

        [OperationContract]
        Account[] GetAllAccounts();

        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        Account UpdateAccount(Account account);


        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void DeleteAccount(int id);



        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "AccountServiceManager.ContractType".
    //[DataContract]
    //public class CompositeType
    //{
    //    bool boolValue = true;
    //    string stringValue = "Hello ";

    //    [DataMember]
    //    public bool BoolValue
    //    {
    //        get { return boolValue; }
    //        set { boolValue = value; }
    //    }

    //    [DataMember]
    //    public string StringValue
    //    {
    //        get { return stringValue; }
    //        set { stringValue = value; }
    //    }
    //}
}
