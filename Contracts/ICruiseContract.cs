﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Domain;
using Domain.SubDomain;

namespace Contracts
{
    [ServiceContract]
    public interface ICruiseContract:IServiceProxyContract
    {
        [OperationContract]
        [FaultContract(typeof(NotFoundException))]
        Cruise GetCruiseById(int id);

        [OperationContract]
        [FaultContract(typeof(NotFoundException))]
        Cruise[] GetCruisesByAccount(int id);

        [OperationContract]
        [FaultContract(typeof(AuthorizationValidationException))]
        void DeletePhoto(Photo Photo);

        [OperationContract]
        [FaultContract(typeof(AuthorizationValidationException))]
        void SetMainPhoto(Photo photo);

        [OperationContract]
        [FaultContract(typeof(AuthorizationValidationException))]
        Cruise GetCruises(string login);

        [OperationContract]
        Cruise[] GetAllCruises(int CurrentPage);

        [OperationContract]
        int GetAllCruisesNumber();

        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        Cruise UpdateCruise(Cruise Cruise);


        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void DeleteCruise(int id);

        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void UpdateStays(Cruise Cruise);
    }
}
