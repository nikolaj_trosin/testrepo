﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    [ServiceContract]
    public interface IFileContract: IServiceProxyContract
    {
        [OperationContract]
        [ServiceKnownType(typeof(System.IO.Stream))]
        void GetFile(FileUploadMessage file);

    }



    [MessageContract(IsWrapped = false)]
    public class FileUploadMessage
    {
        [MessageHeader]
        public string FileName;
        [MessageHeader]
        public int AccountId;
        [MessageHeader]
        public string AuthCode;
        [MessageBodyMember(Order = 1)]
        public Stream FileByteStream;
    }
}
