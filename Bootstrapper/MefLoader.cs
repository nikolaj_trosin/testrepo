﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition.Hosting;
using DataAccess; 


namespace Bootstrapper
{
    public class MefLoader
    {
        public static CompositionContainer Init()
        {
            AggregateCatalog catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new AssemblyCatalog(typeof(AccountRepository).Assembly));
          // catalog.Catalogs.Add(new AssemblyCatalog(typeof(CruiseRepository).Assembly));

            CompositionContainer container = new CompositionContainer(catalog);
            return container;
        }
    }
}
